import 'package:flutter/material.dart';
import 'page1.dart';

void main() {
  runApp(
    MaterialApp(
      home: Test(),
      debugShowCheckedModeBanner: false,
    ),
  );
}

class Test extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  String valueChoose;
  List listItem = [
    'item 1',
    'item 2',
    'item 3',
    'item 4',
    'item 5',
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
            child: SafeArea(
              child: Container(
                height: 130,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Stack(
                  children: [
                    Positioned(
                      bottom: 45.0,
                      child: Container(
                        height: 85,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.blue[900],
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 25.0, top: 20.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(
                                Icons.menu,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 40.0,
                              ),
                              Icon(
                                Icons.person,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 20.0,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 5.0),
                                child: Text(
                                  'Hi! James',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 30.0,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 35.0),
                                child: DropdownButton(
                                  hint: Text(
                                    'Location: ',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  icon: Icon(Icons.keyboard_arrow_down_rounded,
                                      color: Colors.white),
                                  value: valueChoose,
                                  onChanged: (newValue) {
                                    setState(() {
                                      valueChoose = newValue;
                                    });
                                  },
                                  items: listItem.map((valueItem) {
                                    return DropdownMenuItem(
                                      value: valueItem,
                                      child: Text(valueItem),
                                    );
                                  }).toList(),
                                ),
                              ),
                              SizedBox(
                                width: 50.0,
                              ),
                              Icon(
                                Icons.notifications_none,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 20,
                      top: 60.0,
                      left: 10.0,
                      right: 10.0,
                      child: Card(
                        elevation: 5.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Container(
                          height: 100,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white,
                          ),
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 30.0, right: 20.0, top: 0.0),
                                child: Text(
                                  'Search',
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 0.0, right: 20.0),
                                child: Icon(
                                  Icons.search,
                                  color: Colors.blue[900],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            preferredSize: Size.fromHeight(200)),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                child: Container(
                  height: 40.0,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.blueGrey[50],
                  ),
                  child: TabBar(
                    indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.black,
                    ),
                    unselectedLabelColor: Colors.grey,
                    labelColor: Colors.white,
                    tabs: [
                      Tab(
                        text: 'Posted',
                      ),
                      Tab(
                        text: 'Rejected',
                      ),
                      Tab(
                        text: 'Pending',
                      ),
                    ],
                  ),
                ),
              ), //tabBar
              Padding(
                padding: const EdgeInsets.only(top: 5.0, right: 10.0, left: 10.0),
                child: Card(
                  color: Colors.white,
                  elevation: 5.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Container(
                    height: 165,
                    width: MediaQuery.of(context).size.width,
                    child: Stack(
                      children: [
                        Positioned(
                          left: 275.0,
                          top: 40.0,
                          child: Container(
                            width: 90.0,
                            height: 80.0,
                            decoration: BoxDecoration(
                              color: Colors.blue[900],
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Center(
                                child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 15.0),
                                  child: Text(
                                    '05',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 25.0,
                                    ),
                                  ),
                                ),
                                Text(
                                  'Requests',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12.0,
                                  ),
                                ),
                              ],
                            )),
                          ),
                        ),
                        Positioned(
                          left: 370,
                          top: 10.0,
                          child: Icon(Icons.more_vert, color: Colors.grey),
                        ),
                        Positioned(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 30.0, right: 15.0, left: 30.0),
                                    child: Text(
                                      '1 month ago',
                                      style: TextStyle(
                                        color: Colors.grey[400],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 30.0, right: 30.0, left: 0.0),
                                    child: Text(
                                      'Hotels',
                                      style: TextStyle(
                                        color: Colors.lightBlue[400],
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 10.0, right: 30.0, left: 30.0),
                                    child: Text(
                                      'Meet me at NYC Square',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 6.0, right: 30.0, left: 30.0),
                                child: Text(
                                  'New york, USA',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0,
                                    right: 30.0,
                                    left: 30.0,
                                    bottom: 20.0),
                                child: Text(
                                  'Lorem ipsum dolor sit amet, consectrrtur\nadispicing elit, sed do eiumsod tempor\nincididunct ut labore et dolore magna aliqua.',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 10.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ), // body1
              Padding(
            padding: const EdgeInsets.only(top: 5.0, right: 10.0, left: 10.0),
            child: Card(
              color: Colors.white,
              elevation: 5.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Container(
                height: 165,
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: [
                    Positioned(
                      left: 275.0,
                      top: 40.0,
                      child: Container(
                        width: 90.0,
                        height: 80.0,
                        decoration: BoxDecoration(
                          color: Colors.blue[900],
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Center(
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 15.0),
                                  child: Text(
                                    '05',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 25.0,
                                    ),
                                  ),
                                ),
                                Text(
                                  'Requests',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12.0,
                                  ),
                                ),
                              ],
                            )),
                      ),
                    ),
                    Positioned(
                      left: 370,
                      top: 10.0,
                      child: Icon(Icons.more_vert, color: Colors.grey),
                    ),
                    Positioned(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 30.0, right: 15.0, left: 30.0),
                                child: Text(
                                  '1 month ago',
                                  style: TextStyle(
                                    color: Colors.grey[400],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 30.0, right: 30.0, left: 0.0),
                                child: Text(
                                  'Hotels',
                                  style: TextStyle(
                                    color: Colors.lightBlue[400],
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0, right: 30.0, left: 30.0),
                                child: Text(
                                  'Meet me at NYC Square',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 6.0, right: 30.0, left: 30.0),
                            child: Text(
                              'New york, USA',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.0,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10.0,
                                right: 30.0,
                                left: 30.0,
                                bottom: 20.0),
                            child: Text(
                              'Lorem ipsum dolor sit amet, consectrrtur\nadispicing elit, sed do eiumsod tempor\nincididunct ut labore et dolore magna aliqua.',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 10.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0, right: 10.0, left: 10.0),
                child: Card(
                  color: Colors.white,
                  elevation: 5.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Container(
                    height: 165,
                    width: MediaQuery.of(context).size.width,
                    child: Stack(
                      children: [
                        Positioned(
                          left: 275.0,
                          top: 40.0,
                          child: Container(
                            width: 90.0,
                            height: 80.0,
                            decoration: BoxDecoration(
                              color: Colors.blue[900],
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Center(
                                child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 15.0),
                                  child: Text(
                                    '05',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 25.0,
                                    ),
                                  ),
                                ),
                                Text(
                                  'Requests',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12.0,
                                  ),
                                ),
                              ],
                            )),
                          ),
                        ),
                        Positioned(
                          left: 370,
                          top: 10.0,
                          child: Icon(Icons.more_vert, color: Colors.grey),
                        ),
                        Positioned(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 30.0, right: 15.0, left: 30.0),
                                    child: Text(
                                      '1 month ago',
                                      style: TextStyle(
                                        color: Colors.grey[400],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 30.0, right: 30.0, left: 0.0),
                                    child: Text(
                                      'Hotels',
                                      style: TextStyle(
                                        color: Colors.lightBlue[400],
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 10.0, right: 30.0, left: 30.0),
                                    child: Text(
                                      'Meet me at NYC Square',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 6.0, right: 30.0, left: 30.0),
                                child: Text(
                                  'New york, USA',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0,
                                    right: 30.0,
                                    left: 30.0,
                                    bottom: 20.0),
                                child: Text(
                                  'Lorem ipsum dolor sit amet, consectrrtur\nadispicing elit, sed do eiumsod tempor\nincididunct ut labore et dolore magna aliqua.',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 10.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0, right: 10.0, left: 10.0),
                child: Card(
                  color: Colors.white,
                  elevation: 5.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Container(
                    height: 165,
                    width: MediaQuery.of(context).size.width,
                    child: Stack(
                      children: [
                        Positioned(
                          left: 275.0,
                          top: 40.0,
                          child: Container(
                            width: 90.0,
                            height: 80.0,
                            decoration: BoxDecoration(
                              color: Colors.blue[900],
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Center(
                                child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 15.0),
                                  child: Text(
                                    '05',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 25.0,
                                    ),
                                  ),
                                ),
                                Text(
                                  'Requests',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12.0,
                                  ),
                                ),
                              ],
                            )),
                          ),
                        ),
                        Positioned(
                          left: 370,
                          top: 10.0,
                          child: Icon(Icons.more_vert, color: Colors.grey),
                        ),
                        Positioned(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 30.0, right: 15.0, left: 30.0),
                                    child: Text(
                                      '1 month ago',
                                      style: TextStyle(
                                        color: Colors.grey[400],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 30.0, right: 30.0, left: 0.0),
                                    child: Text(
                                      'Hotels',
                                      style: TextStyle(
                                        color: Colors.lightBlue[400],
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 10.0, right: 30.0, left: 30.0),
                                    child: Text(
                                      'Meet me at NYC Square',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 12.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 6.0, right: 30.0, left: 30.0),
                                child: Text(
                                  'New york, USA',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13.0,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0,
                                    right: 30.0,
                                    left: 30.0,
                                    bottom: 20.0),
                                child: Text(
                                  'Lorem ipsum dolor sit amet, consectrrtur\nadispicing elit, sed do eiumsod tempor\nincididunct ut labore et dolore magna aliqua.',
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 10.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.blue[900],
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              // ignore: deprecated_member_use
              title: Text('home'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add_circle_outline),
              // ignore: deprecated_member_use
              title: Text('Post AD'),
              backgroundColor: Colors.red,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.camera_alt_outlined),
              // ignore: deprecated_member_use
              title: Text('camera'),
              backgroundColor: Colors.yellow,
            ),
            BottomNavigationBarItem(
              icon: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => page1()),
                    );
                  },
                  child: Icon(Icons.person_outline_outlined)),
              // ignore: deprecated_member_use
              title: Text('profile'),
              backgroundColor: Colors.green,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.menu),
              // ignore: deprecated_member_use
              title: Text('menu'),
              backgroundColor: Colors.amber,
            ),
          ],
        ),
      ),
      length: 3,
      initialIndex: 0,
    );
  }
}
