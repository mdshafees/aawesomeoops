

import 'package:flutter/material.dart';
import 'profile.dart';




// ignore: camel_case_types
class page1 extends StatefulWidget {
  @override
  _page1State createState() => _page1State();
}

// ignore: camel_case_types
class _page1State extends State<page1> {
  String valueChoose;
  List listItem = [
    'item 1',
    'item 2',
    'item 3',
    'item 4',
    'item 5',
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      child: Scaffold(
        appBar: PreferredSize(
            child: SafeArea(
              child: Stack(
                children: [
                  Positioned(
                    child: Container(
                      height: 90,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.blue[900],
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 15.0, right: 25.0, top: 20.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                               Icon(
                                Icons.menu,
                                color: Colors.white,
                              ),
                            SizedBox(
                              width: 40.0,
                            ),
                            Icon(
                              Icons.person,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 20.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text(
                                'Hi! James',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 30.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 35.0),
                              child: DropdownButton(
                                hint: Text(
                                  'Location: ',
                                  style: TextStyle(color: Colors.white),
                                ),
                                icon: Icon(Icons.keyboard_arrow_down_rounded,
                                    color: Colors.white),
                                value: valueChoose,
                                onChanged: (newValue) {
                                  setState(() {
                                    valueChoose = newValue;
                                  });
                                },
                                items: listItem.map((valueItem) {
                                  return DropdownMenuItem(
                                    value: valueItem,
                                    child: Text(valueItem),
                                  );
                                }).toList(),
                              ),
                            ),
                            SizedBox(
                              width: 50.0,
                            ),
                            Icon(
                              Icons.notifications_none,
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            preferredSize: Size.fromHeight(200)),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 3.0),
            child: Card(
              color: Colors.white,
              elevation: 10.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0, right: 5.0, left: 5.0),
                      child: TabBar(
                        unselectedLabelColor: Colors.black,
                        labelColor: Colors.white,
                        indicator: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.lightBlue[500],
                        ),
                        tabs: [
                          Tab(
                            child: Text('Your Chat',
                              style: TextStyle(
                                fontSize: 16.0,
                              ),
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Tab(
                                child: Text('Request',
                                style: TextStyle(
                                  fontSize: 16.0,
                                ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0),
                                child: Container(
                                  width: 34.0,
                                  height: 15.16,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: Colors.blue[900],
                                  ),
                                  child: Tab(
                                    child: Text('70',
                                      style: TextStyle(
                                        fontSize: 12.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20.0,),
                    Padding(
                      padding: const EdgeInsets.only(left: 17.0, right: 17.0),
                      child: Container(
                        height: 43,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[100],
                        ),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 30.0, right: 20.0, top: 0.0),
                              child: Text(
                                'Search',
                                style: TextStyle(
                                  color: Colors.grey[600],
                                ),
                              ),
                            ),
                            Spacer(),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 0.0, right: 20.0),
                              child: Icon(
                                Icons.search,
                                color: Colors.blue[900],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0,),
                      Container(
                        height: 680.0,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color:Colors.white,
                        ),
                        child: TabBarView(
                          children: [
                            _buildListView('Your Chat'),
                            _buildListView('Request'),
                          ],
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.blue[900],
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              // ignore: deprecated_member_use
              title: Text('home'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add_circle_outline),
              // ignore: deprecated_member_use
              title: Text('Post AD'),
              backgroundColor: Colors.red,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.camera_alt_outlined),
              // ignore: deprecated_member_use
              title: Text('camera'),
              backgroundColor: Colors.yellow,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person_outline_outlined),
              // ignore: deprecated_member_use
              title: Text('profile'),
              backgroundColor: Colors.green,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.menu),
              // ignore: deprecated_member_use
              title: Text('menu'),
              backgroundColor: Colors.amber,
            ),
          ],
        ),
      ),
      length: 2,
      initialIndex: 0,
    );
  }
}

ListView _buildListView(String s) {
  return ListView.builder(
    itemCount: 10,
      itemBuilder:(_, index){
    return ListTile(
      title: Text('Jhone Doe'),
      subtitle: Text('40 min ago',
      style: TextStyle(
        color: Colors.lightBlue,
        fontSize: 12.0,
      ),
      ),
      leading: Image.asset('assets/abcd1234.png'),
      trailing: GestureDetector(
        onTap: () {
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => profile(
            imagePath: Image.asset('assets/abcd1234.png').getImagePath(),
            title: Text('Jhone Doe').getTitle(),
            desc:Text('40 min ago').getDesc(),
          )),);
        },
          child: Icon(Icons.arrow_right)),

    );
  }
  );
}


}



